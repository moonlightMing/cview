module gitlab.com/tslocum/cview

go 1.12

require (
	github.com/gdamore/tcell/v2 v2.1.1-0.20210125004847-19e17097d8fe
	github.com/lucasb-eyer/go-colorful v1.2.0
	github.com/mattn/go-runewidth v0.0.10
	github.com/rivo/uniseg v0.2.0
	gitlab.com/tslocum/cbind v0.1.4
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	golang.org/x/text v0.3.5 // indirect
)
